const int mosituresensor1 = 6;

unsigned int MS1 = 0;

char cmd;

int mydata[3];

//char sensID = 'A';
//float LOWER_THRESHOLD = 14.84;
//float HIGHER_THRESHOLD = 19.84;

//char sensID = 'B';
//float LOWER_THRESHOLD = 14.84;
//float HIGHER_THRESHOLD = 19.84;

//char sensID = 'C';
//float LOWER_THRESHOLD = 10.84;
//float HIGHER_THRESHOLD = 15.84;

//char sensID = 'D';
//float LOWER_THRESHOLD = 18.84;
//float HIGHER_THRESHOLD = 23.84;

char sensID = 'E';
float LOWER_THRESHOLD = 13.84;
float HIGHER_THRESHOLD = 18.84;

float volume_Moist_Percent;

int device_start_flag = 1;

int above_high_threshold_flag = 0;
int below_low_threshold_flag = 0;

int increasing_order_flag = 0;
int decreasing_order_flag = 1;

int low_threshold_touch_flag = 0;
int high_threshold_touch_flag = 0;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  if (Serial.available())              // wait for hardware UART
  {
    cmd = Serial.read();
    if (cmd == sensID)
    {
      moistRead();
    }
  }

  while (Serial.available() == 0)
  {
    //do nothing
  }

}


void moistRead()
{
  int i;
  int mositure1value;

  for (i = 0; i < 3; i++)
  {
    mydata[i] = 's';
  }

  mositure1value = analogRead(mosituresensor1);

  MS1 = mositure1value;

  if (MS1 > 766) {
    MS1 = 766;
  }

  volume_Moist_Percent = (3830 - (5 * (MS1))) / 54;

  mydata[0] = volume_Moist_Percent / 100;
  mydata[1] = (volume_Moist_Percent - (mydata[0] * 100)) / 10;
  mydata[2] = volume_Moist_Percent - ((mydata[0] * 100) + mydata[1] * 10 );

  Serial.print(sensID);

  if (volume_Moist_Percent <= LOWER_THRESHOLD) {
    Serial.print('1');
    below_low_threshold_flag = 1;

    if (above_high_threshold_flag == 1 || device_start_flag == 1) {
      above_high_threshold_flag = 0;
      low_threshold_touch_flag = 1;
      device_start_flag = 0;
    }
    else {
      low_threshold_touch_flag = 0;
    }

    if (low_threshold_touch_flag == 1) {
      increasing_order_flag = 1;
      decreasing_order_flag = 0;
    }
  }

  if (volume_Moist_Percent > LOWER_THRESHOLD && volume_Moist_Percent < HIGHER_THRESHOLD) {
    if (increasing_order_flag == 1) {
      Serial.print('1');
      //decreasing_order_flag = 0;
    }

    if (decreasing_order_flag == 1) {
      Serial.print('0');
      //increasing_order_flag = 1;
    }
  }

  if (volume_Moist_Percent >= HIGHER_THRESHOLD) {
    Serial.print('0');
    above_high_threshold_flag = 1;

    if (below_low_threshold_flag == 1 || device_start_flag == 1) {
      below_low_threshold_flag = 0;
      high_threshold_touch_flag = 1;
      device_start_flag = 0;
    }
    else {
      high_threshold_touch_flag = 0;
    }

    if (high_threshold_touch_flag == 1) {
      increasing_order_flag = 0;
      decreasing_order_flag = 1;
    }
  }

  for (i = 0; i < 3; i++)
  {
    Serial.print(mydata[i]);
  }
}

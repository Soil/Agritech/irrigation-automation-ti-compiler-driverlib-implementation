/*
 * gsm.c
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary */


#include <stdint.h>
#include <stdbool.h>
#include <gsm.h>
#include <string.h>
#include <controller.h>
#include <driverlib.h>

#define EUART_BUFFER_SIZE 512

#define HIGH    1
#define LOW     0


// for circular buffer
volatile char euart_rx_buffer[EUART_BUFFER_SIZE];   // the actual buffer, now 2048 bytes long

uint16_t euart_rx_buffer_rd_ptr = 0;            // the current read position
uint16_t euart_rx_buffer_wr_ptr = 0;            // the current write position


volatile char rx_char;
volatile char tmp_uart_rx_plus_buffer[6];
volatile char tmp_uart_rx_asterisk_buffer[4];
volatile char tmp_uart_rx_hash_buffer[10];

uint8_t tmp_uart_rx_plus_wr_ptr = 0;
uint8_t tmp_uart_rx_asterisk_wr_ptr = 0;
uint8_t tmp_uart_rx_hash_wr_ptr = 0;

volatile uint8_t URC_PLUS_RECEIVED_FLAG = LOW;
volatile uint8_t URC_ASTERISK_RECEIVED_FLAG = LOW;
volatile uint8_t URC_HASH_RECEIVED_FLAG = LOW;

volatile uint8_t SMS_RECEIVED_FLAG = LOW;

volatile uint8_t STOP_MODE_FLAG = HIGH; // Manual mode scheduling
volatile uint8_t SEMI_AUTOMATIC_MODE_FLAG = LOW; // Manual mode with moisture assist scheduling
volatile uint8_t AUTOMATIC_MODE_FLAG = LOW; // Volumetric Moisture based scheduling

char MQTT_PACKET[26];


// Baud rate set to 9600
const eUSCI_UART_Config uartConfigGSM =
{
        EUSCI_A_UART_CLOCKSOURCE_SMCLK,                 // SMCLK Clock Source
        156,                                            // BRDIV = 156
        4,                                              // UCxBRF = 4
        0,                                              // UCxBRS = 0
        EUSCI_A_UART_NO_PARITY,                         // No Parity
        EUSCI_A_UART_LSB_FIRST,                         // LSB First
        EUSCI_A_UART_ONE_STOP_BIT,                      // One stop bit
        EUSCI_A_UART_MODE,                              // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION   // Oversampling ON
};


void Uart_GSM_PortInit(void)
{
    /* Configuring UART Module */
    MAP_UART_initModule(EUSCI_A2_BASE, &uartConfigGSM);

    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A2_BASE);

    /* Enabling interrupts on receiving data */
    MAP_UART_enableInterrupt(EUSCI_A2_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    MAP_Interrupt_enableInterrupt(INT_EUSCIA2);
}


void __delay_GSM(uint32_t milliSec)
{
    uint32_t counter = 0;
    uint32_t cycles = 0;

    cycles = (milliSec * 48000) / 15;

    for (counter = 0; counter <= cycles; counter++) {
        // no operation
    }
}


void GSM_Send(const char *str)
{
    while(*str) {
        MAP_UART_transmitData(EUSCI_A2_BASE, *str++);
    }
}


bool GSM_Init(void)
{
    if(MAP_GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN6) == 0) {
        printf("switching on GSM\n");
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN7);
        __delay_GSM(3000); // 3000ms delay
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN7);
        __delay_GSM(1000); // 1000ms delay
    }

    Uart_GSM_PortInit();

    return true;
}


bool GSM_Setup(void)
{
    GSM_Send("ATE0\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CSCS=\"GSM\"\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CMGF=1\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CSMP=17,167,0,241\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CNMI=2,1,0,0,0\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CPMS=\"SM\"\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CSDH=0\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT+CSAS=0\r\n");
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("AT&W\r\n");
    __delay_GSM(1000); // 1000ms delay
    SMS_Delete();

    return true;
}


bool GSM_Status_Checker(void)
{
    if (MAP_GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN6) != 1) {
        GSM_Init();
        __delay_GSM(10000); // 10000ms delay

        if (MAP_GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN6) == 1) {
            GSM_Setup();
            MQTT_Init();
        }

        return true;
    }

    else {
        return true;
    }
}


bool SMS_Read(void)
{
    //printf("In SMS_Read");
    if (SMS_RECEIVED_FLAG == HIGH) {
        //printf("Reading SMS");
        GSM_Send("AT+CMGR=1,0\r\n");
        __delay_GSM(500); // 500ms delay
        SMS_RECEIVED_FLAG = LOW;
        SMS_Delete();
    }

    return true;
}


bool SMS_Send(char* phoneNumber, char* message)
{
    GSM_Send("AT+CMGS=");
    GSM_Send("\"");
    GSM_Send(phoneNumber);
    GSM_Send("\"");
    GSM_Send("\r\n");
    __delay_GSM(500); // 500ms delay
    GSM_Send(message);
    __delay_GSM(500); // 500ms delay
    //GSM_Send(0x1A); // ctrl+z substitute character
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint_fast8_t) 0x1A);
    __delay_GSM(3000); // 3000ms delay

    return true;
}


bool SMS_Delete(void)
{
    GSM_Send("AT+CMGD=1\r\n");
    __delay_GSM(500); // 500ms delay

    return true;
}

/*
bool gsmReset(void)
{
    //DO SOMETHING
}


uint16_t rtcHours(void)
{

}


uint16_t rtcMinutes(void)
{

}


uint16_t rtcDate(void)
{

}


uint16_t rtcYear(void)
{

}

*/
bool MQTT_Init()
{
    /* Open MQTT connection */
    GSM_Send("AT+QMTOPEN=0,\"104.198.201.218\",1883\r\n");
    __delay_GSM(4000); // 4000ms delay

    /* Connecting to MQTT broker */
    GSM_Send("AT+QMTCONN=0,\"TEST4\",\"hv.9977238645@gmail.com\",\"b2c8fa59\"\r\n");
    __delay_GSM(5000); // 5000ms delay

    return true;
}


bool MQTT_Publish(void)
{
    uint16_t index = 0;

    GSM_Send("AT+QMTPUB=0,0,0,0,\"/hv.9977238645@gmail.com/moist\"\r\n");
    __delay_GSM(500); // 500ms delay
    while (MQTT_PACKET[index] != '\0') {
        //GSM_Send(MQTT_PACKET[index]);
        MAP_UART_transmitData(EUSCI_A2_BASE, MQTT_PACKET[index]);
        index++;
    }
    __delay_GSM(1000); // 1000ms delay
    GSM_Send("\r\n");
    __delay_GSM(1000); // 1000ms delay
    //GSM_Send(0x1A); // ctrl+z substitute character
    MAP_UART_transmitData(EUSCI_A2_BASE, (uint_fast8_t) 0x1A);
    __delay_GSM(4000); // 4000ms delay

    return true;
}


/*
bool MQTT_Subscribe()
{

}
*/


/* EUSCI A2 UART ISR */
void EUSCIA2_IRQHandler(void)
{
    uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A2_BASE);

    //printf("In GSM uart INT handler \n");

    MAP_UART_clearInterruptFlag(EUSCI_A2_BASE, status);

    if(status & EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG)
    {
        // circular buffer
        euart_rx_buffer[euart_rx_buffer_wr_ptr++] = MAP_UART_receiveData(EUSCI_A2_BASE);  // Store the received data

        if (euart_rx_buffer_wr_ptr >= EUART_BUFFER_SIZE) {    // Increment write pointer
            euart_rx_buffer_wr_ptr = 0;
        }
        //printf("%c\n", rx_char);
    }
}



// Reading from circular buffer
void GSM_URC_Read(void)
{
    while (euart_rx_buffer_rd_ptr != euart_rx_buffer_wr_ptr) {   // While there's data in the buffer
        rx_char  = euart_rx_buffer[euart_rx_buffer_rd_ptr++];

        //printf("%d\n", URC_ASTERISK_RECEIVED_FLAG);

        if (rx_char == '+') {
            //printf("+ received\n");
            URC_PLUS_RECEIVED_FLAG = HIGH;
            URC_ASTERISK_RECEIVED_FLAG = LOW;
            URC_HASH_RECEIVED_FLAG = LOW;
        }

        if (rx_char == '*') {
            //printf("* received\n");
            URC_PLUS_RECEIVED_FLAG = LOW;
            URC_ASTERISK_RECEIVED_FLAG = HIGH;
            URC_HASH_RECEIVED_FLAG = LOW;
        }

        if (rx_char == '#') {
            //printf("# received\n");
            URC_PLUS_RECEIVED_FLAG = LOW;
            URC_ASTERISK_RECEIVED_FLAG = LOW;
            URC_HASH_RECEIVED_FLAG = HIGH;
        }


        if (URC_PLUS_RECEIVED_FLAG == HIGH) {

            tmp_uart_rx_plus_buffer[tmp_uart_rx_plus_wr_ptr++] = rx_char;

            if (tmp_uart_rx_plus_wr_ptr == 5) {

                tmp_uart_rx_plus_buffer[tmp_uart_rx_plus_wr_ptr] = '\0';

                if (strcmp(tmp_uart_rx_plus_buffer, "+CMTI") == 0) {
                    //printf("CMTI received\n");
                    SMS_RECEIVED_FLAG = HIGH;
                }

                tmp_uart_rx_plus_wr_ptr = 0;
                URC_PLUS_RECEIVED_FLAG = LOW;
            }
        }


        if (URC_ASTERISK_RECEIVED_FLAG == HIGH) {

            tmp_uart_rx_asterisk_buffer[tmp_uart_rx_asterisk_wr_ptr++] = rx_char;

            if (tmp_uart_rx_asterisk_wr_ptr == 3) {

                tmp_uart_rx_plus_buffer[tmp_uart_rx_plus_wr_ptr] = '\0';

                if (strcmp(tmp_uart_rx_asterisk_buffer, "*a0") == 0) {
                    //printf("a0 received\n");
                    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
                    AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_asterisk_buffer, "*a1") == 0) {
                    //printf("a1 received\n");
                    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(HIGH);
                    AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(HIGH);
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_asterisk_buffer, "*m0") == 0) {
                    //printf("m0 received\n");
                    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
                    MDC_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_asterisk_buffer, "*m1") == 0) {
                    //printf("m1 received\n");
                    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(HIGH);
                    MDC_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(HIGH);
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_asterisk_buffer, "*b0") == 0) {
                    //printf("b0 received\n");
                    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
                    MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_asterisk_buffer, "*b1") == 0) {
                    //printf("b1 received\n");
                    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(HIGH);
                    MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(HIGH);
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                tmp_uart_rx_asterisk_wr_ptr = 0;
                URC_ASTERISK_RECEIVED_FLAG = LOW;
            }
        }


        if (URC_HASH_RECEIVED_FLAG == HIGH) {

            tmp_uart_rx_hash_buffer[tmp_uart_rx_hash_wr_ptr++] = rx_char;

            if (tmp_uart_rx_hash_wr_ptr == 9) {

                tmp_uart_rx_hash_buffer[tmp_uart_rx_hash_wr_ptr] = '\0';

                if (strcmp(tmp_uart_rx_hash_buffer, "#soilauto") == 0) {
                    //printf("soilauto received\n");
                    AUTOMATIC_MODE_FLAG = HIGH;
                    SEMI_AUTOMATIC_MODE_FLAG = LOW;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_hash_buffer, "#soilsemi") == 0) {
                    //printf("soilsemi received\n");
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = HIGH;
                    STOP_MODE_FLAG = LOW;
                }

                if (strcmp(tmp_uart_rx_hash_buffer, "#soil-off") == 0) {
                    //printf("soil-off received\n");
                    AUTOMATIC_MODE_FLAG = LOW;
                    SEMI_AUTOMATIC_MODE_FLAG = LOW;
                    STOP_MODE_FLAG = HIGH;
                }

                tmp_uart_rx_hash_wr_ptr = 0;
                URC_HASH_RECEIVED_FLAG = LOW;
            }
        }

        if (euart_rx_buffer_rd_ptr >= EUART_BUFFER_SIZE) {     // Increase read pointer
            euart_rx_buffer_rd_ptr = 0;
        }
    }
}


uint8_t STOP_MODE_FLAG_STATUS(void)
{
    return STOP_MODE_FLAG;
}


uint8_t SEMI_AUTOMATIC_MODE_FLAG_STATUS(void)
{
    return SEMI_AUTOMATIC_MODE_FLAG;
}


uint8_t AUTOMATIC_MODE_FLAG_STATUS(void)
{
    return AUTOMATIC_MODE_FLAG;
}


void STOP_MODE_FLAG_UPDATE(uint8_t command)
{
    STOP_MODE_FLAG = command;
}


void SEMI_AUTOMATIC_MODE_FLAG_UPDATE(uint8_t command)
{
    SEMI_AUTOMATIC_MODE_FLAG = command;
}


void AUTOMATIC_MODE_FLAG_UPDATE(uint8_t command)
{
    AUTOMATIC_MODE_FLAG = command;
}

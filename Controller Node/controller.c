/*
 * controller.c
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */

#define HIGH        1
#define LOW         0

#include <stdint.h>
#include <stdbool.h>
#include <controller.h>
#include <gsm.h>
#include <lcd.h>
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>


uint8_t SOLENOID_IN_USE_FLAG = LOW;

/* Pump ON/OFF notification and SMS Acknowledgement flags */
uint8_t PUMP_ON_NOTIFICATION_FLAG = LOW;
uint8_t PUMP_OFF_NOTIFICATION_FLAG = LOW;

uint8_t PUMP_ON_SMS_ACK_FLAG = LOW;
uint8_t PUMP_OFF_SMS_ACK_FLAG = LOW;



/* Solenoid ON/OFF notification and SMS Acknowledgement flags */
uint8_t AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
uint8_t AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
uint8_t MDC_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
uint8_t MDC_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
uint8_t MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
uint8_t MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;

uint8_t AMPHI_SOLENOID_ON_SMS_ACK_FLAG = LOW;
uint8_t AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
uint8_t MDC_SOLENOID_ON_SMS_ACK_FLAG = LOW;
uint8_t MDC_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
uint8_t MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = LOW;
uint8_t MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = LOW;

uint8_t PUMP_SWITCH_MANUAL_TOGGLE_FLAG = LOW;
uint8_t AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG = LOW;
uint8_t MDC_SOLENOID_MANUAL_TOGGLE_FLAG = LOW;
uint8_t MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG = LOW;

/* Declaring flags*/
static uint8_t PUMP_STATUS_FLAG = 0;
static uint8_t MDC_SOLENOID_STATUS_FLAG = 0;
static uint8_t MDC_BACK_SOLENOID_STATUS_FLAG = 0;
static uint8_t AMPHI_SOLENOID_STATUS_FLAG = 0;

static char USER_PHONE_NUMBER[] = "+918349504911";

void __delay_Controller(uint32_t milliSec)
{
    uint32_t counter = 0;
    uint32_t cycles = 0;

    cycles = (milliSec * 48000) / 15;

    for (counter = 0; counter <= cycles; counter++) {
        // no operation
    }
}


bool Pump_Switch(uint8_t command)
{
    if (command == PUMP_ON && PUMP_STATUS_FLAG == 0) {
        if (MDC_SOLENOID_STATUS_FLAG == 1 || MDC_BACK_SOLENOID_STATUS_FLAG == 1 || AMPHI_SOLENOID_STATUS_FLAG == 1) {
            //printf("Pump switch ON\n");
            MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN2);
            __delay_Controller(2500); // 2500ms delay
            MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN2);

            PUMP_STATUS_FLAG = 1;
            PUMP_ON_NOTIFICATION_FLAG = HIGH;
            PUMP_OFF_NOTIFICATION_FLAG = LOW;
        }
    }

    if (command == PUMP_OFF && PUMP_STATUS_FLAG == 1) {
        //printf("Pump switch OFF\n");
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN0);
        __delay_Controller(7000); // 7000ms delay
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0);

        PUMP_STATUS_FLAG = 0;
        PUMP_OFF_NOTIFICATION_FLAG = HIGH;
        PUMP_ON_NOTIFICATION_FLAG = LOW;
    }

    return true;
}


bool MDC_Solenoid_Switch(uint8_t command)
{
    if (command == MDC_SOLENOID_ON && MDC_SOLENOID_STATUS_FLAG == 0) {
        //printf("MDC switch ON\n");
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN6);

        MDC_SOLENOID_STATUS_FLAG = 1;

        AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_ON_NOTIFICATION_FLAG = HIGH;
        MDC_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
    }

    if (command == MDC_SOLENOID_OFF && MDC_SOLENOID_STATUS_FLAG == 1) {
        //printf("MDC switch OFF\n");
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN6);

        MDC_SOLENOID_STATUS_FLAG = 0;

        AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_OFF_NOTIFICATION_FLAG = HIGH;
        MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
    }

    return true;
}


bool MDC_Back_Solenoid_Switch(uint8_t command)
{
    if (command == MDC_BACK_SOLENOID_ON && MDC_BACK_SOLENOID_STATUS_FLAG == 0) {
        //printf("MDC back switch ON\n");
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN7);

        MDC_BACK_SOLENOID_STATUS_FLAG = 1;

        AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = HIGH;
        MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
    }

    if (command == MDC_BACK_SOLENOID_OFF && MDC_BACK_SOLENOID_STATUS_FLAG == 1) {
        //printf("MDC back switch OFF\n");
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN7);

        MDC_BACK_SOLENOID_STATUS_FLAG = 0;

        AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = HIGH;
    }

    return true;
}


bool Amphi_Solenoid_Switch(uint8_t command)
{
    if (command == AMPHI_SOLENOID_ON && AMPHI_SOLENOID_STATUS_FLAG == 0) {
        //printf("Amphi switch ON\n");
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN7);

        AMPHI_SOLENOID_STATUS_FLAG = 1;

        AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = HIGH;
        AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
    }

    if (command == AMPHI_SOLENOID_OFF && AMPHI_SOLENOID_STATUS_FLAG == 1) {
        //printf("Amphi switch OFF\n");
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN7);

        AMPHI_SOLENOID_STATUS_FLAG = 0;

        AMPHI_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG = HIGH;
        MDC_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG = LOW;
        MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG = LOW;
    }

    return true;
}


uint8_t MDC_SOLENOID_STATUS(void)
{
    return MDC_SOLENOID_STATUS_FLAG;
}


uint8_t MDC_BACK_SOLENOID_STATUS(void)
{
    return MDC_BACK_SOLENOID_STATUS_FLAG;
}


void Pump_Status_Checker(void)
{
    if (PUMP_ON_NOTIFICATION_FLAG == HIGH) {
        if (PUMP_ON_SMS_ACK_FLAG == LOW) {
            //printf("sending Pump ON ack sms\n");
            SMS_Send(USER_PHONE_NUMBER, "Pump ON");
            PUMP_ON_SMS_ACK_FLAG = HIGH;
            PUMP_OFF_SMS_ACK_FLAG = LOW;
        }
    }

    if (PUMP_OFF_NOTIFICATION_FLAG == HIGH) {
        if (PUMP_OFF_SMS_ACK_FLAG == LOW) {
            //printf("sending Pump OFF ack sms\n");
            SMS_Send(USER_PHONE_NUMBER, "Pump OFF");
            PUMP_OFF_SMS_ACK_FLAG = HIGH;
            PUMP_ON_SMS_ACK_FLAG = LOW;
        }
    }
}


void Solenoid_Status_Checker(void)
{
    //printf("In Solenoid_Status_Checker\n");
    if (MDC_SOLENOID_ON_NOTIFICATION_FLAG == HIGH) {
        if (MDC_SOLENOID_ON_SMS_ACK_FLAG == LOW) {
            //printf("sending mdc on ack sms\n");
            lcdPrint("  ON  ", 7, 2);
            SMS_Send(USER_PHONE_NUMBER, "MDC ON");

            AMPHI_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_ON_SMS_ACK_FLAG = HIGH;
            MDC_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
        }
    }

    if (MDC_SOLENOID_OFF_NOTIFICATION_FLAG == HIGH) {
        if (MDC_SOLENOID_OFF_SMS_ACK_FLAG == LOW) {
            //printf("sending mdc off ack sms\n");
            lcdPrint(" OFF  ", 7, 2);
            SMS_Send(USER_PHONE_NUMBER, "MDC OFF");

            AMPHI_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_OFF_SMS_ACK_FLAG = HIGH;
            MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
        }
    }

    if (MDC_BACK_SOLENOID_ON_NOTIFICATION_FLAG == HIGH) {
        if (MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG == LOW) {
            //printf("sending mdc back on ack sms\n");
            lcdPrint("  ON  ", 14, 2);
            SMS_Send(USER_PHONE_NUMBER, "MDC Back ON");

            AMPHI_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = HIGH;
            MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
        }
    }


    if (MDC_BACK_SOLENOID_OFF_NOTIFICATION_FLAG == HIGH) {
        if (MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG == LOW) {
            //printf("sending mdc back off ack sms\n");
            lcdPrint(" OFF  ", 14, 2);
            SMS_Send(USER_PHONE_NUMBER, "MDC Back OFF");

            AMPHI_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = HIGH;
        }
    }

    if (AMPHI_SOLENOID_ON_NOTIFICATION_FLAG == HIGH) {
        if (AMPHI_SOLENOID_ON_SMS_ACK_FLAG == LOW) {
            //printf("sending amphi on ack sms\n");
            lcdPrint("  ON  ", 0, 2);
            SMS_Send(USER_PHONE_NUMBER, "AMPHI ON");

            AMPHI_SOLENOID_ON_SMS_ACK_FLAG = HIGH;
            AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
        }
    }

    if (AMPHI_SOLENOID_OFF_NOTIFICATION_FLAG == HIGH) {
        if (AMPHI_SOLENOID_OFF_SMS_ACK_FLAG == LOW) {
            //printf("sending amphi off ack sms\n");
            lcdPrint(" OFF  ", 0, 2);
            SMS_Send(USER_PHONE_NUMBER, "AMPHI OFF");

            AMPHI_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            AMPHI_SOLENOID_OFF_SMS_ACK_FLAG = HIGH;
            MDC_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_ON_SMS_ACK_FLAG = LOW;
            MDC_BACK_SOLENOID_OFF_SMS_ACK_FLAG = LOW;
        }
    }
}


void SOLENOID_IN_USE_FLAG_UPDATE(uint8_t command)
{
    SOLENOID_IN_USE_FLAG = command;
}


uint8_t SOLENOID_IN_USE_FLAG_STATUS(void)
{
    return SOLENOID_IN_USE_FLAG;
}


void PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command)
{
    PUMP_SWITCH_MANUAL_TOGGLE_FLAG = command;
}


void AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command)
{
    AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG = command;
}


void MDC_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command)
{
    MDC_SOLENOID_MANUAL_TOGGLE_FLAG = command;
}


void MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command)
{
    MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG = command;
}


uint8_t PUMP_SWITCH_MANUAL_TOGGLE_FLAG_STATUS(void)
{
    return PUMP_SWITCH_MANUAL_TOGGLE_FLAG;
}


uint8_t AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_STATUS(void)
{
    return AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG;
}


uint8_t MDC_SOLENOID_MANUAL_TOGGLE_FLAG_STATUS(void)
{
    return MDC_SOLENOID_MANUAL_TOGGLE_FLAG;
}


uint8_t MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_STATUS(void)
{
    return MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG;
}


void Semi_Automatic_Mode_Control_Check()
{
    //printf("In Semi_Automatic_Mode_Control_Check");

    if (SEMI_AUTOMATIC_MODE_FLAG_STATUS() == HIGH) {
        //printf("SEMI_AUTOMATIC_MODE on\n");

        if (AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG == LOW){
            //printf("manual amphi off\n");
            Amphi_Solenoid_Switch(OFF);
            SOLENOID_IN_USE_FLAG_UPDATE(LOW); // To avoid hindrance when switching to automatic mode
        }

        if (AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG == HIGH){
            //printf("manual amphi on\n");
            Amphi_Solenoid_Switch(ON);
            SOLENOID_IN_USE_FLAG_UPDATE(HIGH); // To avoid hindrance when switching to automatic mode
        }

        if (MDC_SOLENOID_MANUAL_TOGGLE_FLAG == LOW){
            //printf("manual mdc off\n");
            MDC_Solenoid_Switch(OFF);
            SOLENOID_IN_USE_FLAG_UPDATE(LOW); // To avoid hindrance when switching to automatic mode
        }

        if (MDC_SOLENOID_MANUAL_TOGGLE_FLAG == HIGH){
            //printf("manual mdc on\n");
            MDC_Solenoid_Switch(ON);
            SOLENOID_IN_USE_FLAG_UPDATE(HIGH); // To avoid hindrance when switching to automatic mode
        }

        if (MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG == LOW){
            //printf("manual mdc back off\n");
            MDC_Back_Solenoid_Switch(OFF);
            SOLENOID_IN_USE_FLAG_UPDATE(LOW); // To avoid hindrance when switching to automatic mode
        }

        if (MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG == HIGH){
            //printf("manual mdc back on\n");
            MDC_Back_Solenoid_Switch(ON);
            SOLENOID_IN_USE_FLAG_UPDATE(HIGH); // To avoid hindrance when switching to automatic mode
        }

        if (PUMP_SWITCH_MANUAL_TOGGLE_FLAG == LOW){
            //printf("manual pump off\n");
            Pump_Switch(OFF);
        }

        if (PUMP_SWITCH_MANUAL_TOGGLE_FLAG == HIGH){
            //printf("manual pump on\n");
            Pump_Switch(ON);
        }
    }
}


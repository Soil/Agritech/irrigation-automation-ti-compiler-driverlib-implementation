/*
 * timeCalc.c
 *
 *  Created on: Jul 11, 2018
 *      Author: Aaditya Chaudhary
 */
#include <timeCalc.h>
#include <stdint.h>
#include <stdbool.h>
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

uint16_t totalMinutes(uint8_t hours, uint8_t minutes)
{
    return ((hours * 60) + minutes);
}


bool timeCompare(uint16_t time1, uint16_t time2)
{
    if (time1 >= time2) {
        return true;
    }

    else {
        return false;
    }
}



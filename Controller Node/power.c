/*
 * power.c
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */
#include <power.h>

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>

#include <driverlib.h>

/* Statics */
static volatile uint16_t curADCResult;
static volatile float normalizedADCRes;


void __delay_Power(uint32_t milliSec)
{
    uint32_t counter = 0;
    uint32_t cycles = 0;

    cycles = (milliSec * 48000) / 15;

    for (counter = 0; counter <= cycles; counter++) {
        // no operation
    }
}


void ADC_Init(void)
{
    // Initializing Variables
    curADCResult = 0;

    // set to unrestricted power mode
    MAP_ADC14_setPowerMode(ADC_UNRESTRICTED_POWER_MODE);

    //![Single Sample Mode Configure]
    // Initializing ADC (MCLK/1/4)
    MAP_ADC14_enableModule();
    MAP_ADC14_initModule(ADC_CLOCKSOURCE_MCLK, ADC_PREDIVIDER_1, ADC_DIVIDER_4, ADC_NOROUTE);

    // Configuring ADC Memory
    MAP_ADC14_configureSingleSampleMode(ADC_MEM0, true);
    MAP_ADC14_configureConversionMemory(ADC_MEM0, ADC_VREFPOS_AVCC_VREFNEG_VSS,ADC_INPUT_A15, ADC_NONDIFFERENTIAL_INPUTS);

    // Configuring the sample/hold time
    MAP_ADC14_setSampleHoldTime(ADC_PULSE_WIDTH_192,ADC_PULSE_WIDTH_192);

    // Configuring Sample Timer
    MAP_ADC14_enableSampleTimer(ADC_MANUAL_ITERATION);

    // Setting reference voltage to 2.5
    MAP_REF_A_setReferenceVoltage(REF_A_VREF2_5V);
    MAP_REF_A_enableReferenceVoltage();
}


float cellVoltage(void)
{
    // Enabling/Toggling Conversion
    MAP_ADC14_enableConversion();

    // Take measurement for 3 times to warm up the ADC
    MAP_ADC14_toggleConversionTrigger();
    __delay_Power(40);  //40 ms delay(if 48Mhz clock source)
    MAP_ADC14_toggleConversionTrigger();
    __delay_Power(40); //40 ms delay(if 48Mhz clock source)
    MAP_ADC14_toggleConversionTrigger();
    __delay_Power(40); //40 ms delay(if 48Mhz clock source)

    // disable the ADC module
    MAP_ADC14_disableConversion();

    return normalizedADCRes;
}


bool powerSupply(void)
{
    if ((MAP_GPIO_getInputPinValue(GPIO_PORT_P3, GPIO_PIN6)) == 0x01) {
        return true;
    }
    else {
        return false;
    }
}


//![Single Sample Result]
/* ADC Interrupt Handler. This handler is called whenever there is a conversion
* that is finished for ADC_MEM0.
*/
void ADC14_IRQHandler(void)
{
    uint64_t status = MAP_ADC14_getEnabledInterruptStatus();
    MAP_ADC14_clearInterruptFlag(status);

    if (ADC_INT0 & status) {
        curADCResult = MAP_ADC14_getResult(ADC_MEM0);
        normalizedADCRes = (curADCResult * 4.2) / 16384;
    }
}
//![Single Sample Result]




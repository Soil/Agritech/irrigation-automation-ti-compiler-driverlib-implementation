/*
 * timeCalc.h
 *
 *  Created on: Jul 11, 2018
 *      Author: Aaditya Chaudhary
 */

#ifndef TIMECALC_H_
#define TIMECALC_H_

#include <stdint.h>
#include <stdbool.h>

uint16_t totalMinutes(uint8_t hours, uint8_t minutes);
bool timeCompare(uint16_t time1, uint16_t time2);

#endif /* TIMECALC_H_ */

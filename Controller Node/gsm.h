/*
 * gsm.h
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */

#ifndef GSM_H_
#define GSM_H_

#include <stdint.h>
#include <stdbool.h>

extern char MQTT_PACKET[26];

bool GSM_Init(void);
bool GSM_Setup(void);
bool GSM_Status_Checker(void);
bool SMS_Read(void);
bool SMS_Send(char* phoneNumber, char* message);
bool SMS_Delete(void);
bool gsmReset(void);
char* urcRead(void);
uint16_t rtcHours(void);
uint16_t rtcMinutes(void);
uint16_t rtcDate(void);
uint16_t rtcYear(void);
bool MQTT_Init(void);
bool MQTT_Publish(void);
void GSM_URC_Read(void);
uint8_t STOP_MODE_FLAG_STATUS(void);
uint8_t SEMI_AUTOMATIC_MODE_FLAG_STATUS(void);
uint8_t AUTOMATIC_MODE_FLAG_STATUS(void);
void STOP_MODE_FLAG_UPDATE(uint8_t command);
void SEMI_AUTOMATIC_MODE_FLAG_UPDATE(uint8_t command);
void AUTOMATIC_MODE_FLAG_UPDATE(uint8_t command);

#endif /* GSM_H_ */

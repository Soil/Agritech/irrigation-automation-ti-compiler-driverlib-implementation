/*
 * power.h
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */

#ifndef POWER_H_
#define POWER_H_

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>

//#include <adc14.h>
//#include <gpio.h>

void ADC_Init(void);
float cellVoltage(void);
bool powerSupply(void);
void ADC14_IRQHandler(void);

#endif /* POWER_H_ */

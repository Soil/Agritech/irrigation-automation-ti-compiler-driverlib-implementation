/*****************************************************************/
/*********                                               *********/
/********* AUTOR:    ANTONIO ARMANDO BIZA FERREIRA       *********/
/********* PROJECTO: ECG                                 *********/
/********* DATA:     01-02-2017                          *********/
/********* IDE:      CODE COMPOSER STUDIO V5             *********/
/********* MCU:      MSP432 	                            *********/
/*********                                               *********/
/********* FILE:     LCD.H                               *********/
/*********                                               *********/
/*****************************************************************/

#ifndef LCD_H_
#define LCD_H_

/*****************************************************************/
/********* INCLUDE                                       *********/
/*****************************************************************/

#include <msp432.h>
#include <string.h>
#include <stdio.h>

/*****************************************************************/
/********* LCD Pinout                                    *********/
/*****************************************************************/
/*                                                               */
/*  Pin 1   VSS                                                  */
/*  Pin 2   VDD                                                  */
/*  Pin 3   VO                                                   */
/*  Pin 4   RS                                                   */
/*  Pin 5   RW                                                   */
/*  Pin 6   E                                                    */
/*  Pin 7   D0                                                   */
/*  Pin 8   D1                                                   */
/*  Pin 9   D2                                                   */
/*  Pin 10  D3                                                   */
/*  Pin 11  D4                                                   */
/*  Pin 12  D5                                                   */
/*  Pin 13  D6                                                   */
/*  Pin 14  D7                                                   */
/*  Pin 15  K                                                    */
/*  Pin 16  A                                                    */
/*                                                               */
/*****************************************************************/
#define ADDR_RAM	0x40
#define ROW_ONE		0x80
#define ROW_TWO		0xC0
#define CMD_END		0x01

#define PORT3               0x0300
#define PORT5               0x0500
#define PORT6               0x0600
#define PORT4               0x0400

#define PORT2orPORT5orPORT6orPORT4      (PORT2|PORT5|PORT6|PORT4)
/*PORT2*/
#define P3OFF(x)                P3OUT &= ~(x & 0x00FF)
#define P3ON(x)             P3OUT |=  (x & 0x00FF)
#define P3TOGGLE(x)         P3OUT ^=  (x & 0x00FF)
#define P3DIR2OUT(x)            P3DIR |=  (x & 0x00FF)
#define P3DIR2IN(x)         P3DIR &= ~(x & 0x00FF)
#define P3SEL2ON(x)         P3SEL |=  (x & 0x00FF)
#define P3SEL2OFF(x)            P3SEL &= ~(x & 0x00FF)

/*PORT5*/
#define P5OFF(x)                P5OUT &= ~(x & 0x00FF)
#define P5ON(x)             P5OUT |=  (x & 0x00FF)
#define P5TOGGLE(x)         P5OUT ^=  (x & 0x00FF)
#define P5DIR2OUT(x)            P5DIR |=  (x & 0x00FF)
#define P5DIR2IN(x)         P5DIR &= ~(x & 0x00FF)
#define P5SEL2ON(x)         P5SEL |=  (x & 0x00FF)
#define P5SEL2OFF(x)            P5SEL &= ~(x & 0x00FF)

/*PORT6*/
#define P6OFF(x)                P6OUT &= ~(x & 0x00FF)
#define P6ON(x)             P6OUT |=  (x & 0x00FF)
#define P6TOGGLE(x)         P6OUT ^=  (x & 0x00FF)
#define P6DIR2OUT(x)            P6DIR |=  (x & 0x00FF)
#define P6DIR2IN(x)         P6DIR &= ~(x & 0x00FF)
#define P6SEL2ON(x)         P6SEL |=  (x & 0x00FF)
#define P6SEL2OFF(x)            P6SEL &= ~(x & 0x00FF)

/*PORT4*/
#define P4OFF(x)                P4OUT &= ~(x & 0x00FF)
#define P4ON(x)             P4OUT |=  (x & 0x00FF)
#define P4TOGGLE(x)         P4OUT ^=  (x & 0x00FF)
#define P4DIR2OUT(x)            P4DIR |=  (x & 0x00FF)
#define P4DIR2IN(x)         P4DIR &= ~(x & 0x00FF)
#define P4SEL2ON(x)         P4SEL |=  (x & 0x00FF)
#define P4SEL2OFF(x)            P4SEL &= ~(x & 0x00FF)


#define PORT3MASK(x)            ((x & PORT3) >> 4)
#define PORT5MASK(x)            ((x & PORT5) >> 4)
#define PORT6MASK(x)            ((x & PORT6) >> 4)
#define PORT4MASK(x)            ((x & PORT4) >> 4)

// Delay Functions
//#define OSC_1MHz
#define MHz             1000
#define KHz             1

#ifdef OSC_1MHz
 //  1 MHz
 #define delay_ms(x)     __delay_cycles((long) x * 1000)
 #define delay_us(x)     __delay_cycles((long) x)
 #define XTAL_FREQ      (1*MHz)
#else
 // 24 MHz
 #define delay_ms(x)     __delay_cycles((long) x * 1000 * 48)
 #define delay_us(x)     __delay_cycles((long) x * 48)
 #define XTAL_FREQ      (48*MHz)
#endif


// States
#define LIGADO          1
#define DESLIGADO       0
#define ON              1
#define OFF             0




/*****************************************************************/
/********* FUNCTIONS                                     *********/
/*****************************************************************/

void LCD_PortConfig(void);
void LCD_Initialize(void);
void LCD_SetText(char* text, int x, int y);
void LCD_SetInt(int val, int x, int y);
void LCD_Clear(void);
void LCD_LoadSymbols(void);
void LCD_SetSymbol(unsigned char symbol, unsigned char offset, unsigned char line);
void lcdPrint(char* text, int x, int y);

/*****************************************************************/
/********* ENDIF                                         *********/
/*****************************************************************/
#endif




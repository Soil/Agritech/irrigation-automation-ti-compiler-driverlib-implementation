
 /* Author: Aaditya Chaudhary */
/*******************************************************************************/


/* DriverLib Includes */
#include <driverlib.h>

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>
#include <ports.h>
#include <power.h>
#include <timer.h>
#include <lcd.h>
#include <gsm.h>
#include <lora.h>
#include <controller.h>
#include <timer_a.h>
#include <timer32.h>

#define HIGH        1
#define LOW         0

#define ON          1
//#define OFF         0


volatile uint8_t MQTT_UPLOAD_PERMISSION_FLAG = LOW;

/* Dry/Wet notification and SMS Acknowledgement flags */
uint8_t AMPHI_DRY_NOTIFICATION_FLAG = LOW;
uint8_t AMPHI_WET_NOTIFICATION_FLAG = LOW;
uint8_t MDC_DRY_NOTIFICATION_FLAG = LOW;
uint8_t MDC_WET_NOTIFICATION_FLAG = LOW;
uint8_t MDC_BACK_DRY_NOTIFICATION_FLAG = LOW;
uint8_t MDC_BACK_WET_NOTIFICATION_FLAG = LOW;

uint8_t AMPHI_DRY_SMS_ACK_FLAG = LOW;
uint8_t AMPHI_WET_SMS_ACK_FLAG = LOW;
uint8_t MDC_DRY_SMS_ACK_FLAG = LOW;
uint8_t MDC_WET_SMS_ACK_FLAG = LOW;
uint8_t MDC_BACK_DRY_SMS_ACK_FLAG = LOW;
uint8_t MDC_BACK_WET_SMS_ACK_FLAG = LOW;

char *PTR_DEVICE_A_DATA;
char *PTR_DEVICE_B_DATA;
char *PTR_DEVICE_C_DATA;
char *PTR_DEVICE_D_DATA;
char *PTR_DEVICE_E_DATA;

char DEVICE_A_SENS_VALUE[3];
char DEVICE_B_SENS_VALUE[3];
char DEVICE_C_SENS_VALUE[3];
char DEVICE_D_SENS_VALUE[3];
char DEVICE_E_SENS_VALUE[3];

uint16_t Sensor_Send_Counter = 0;

uint8_t POWER_SUPPLY_AVAIL_SMS_ACK_FLAG = HIGH;
uint8_t POWER_SUPPLY_NOT_AVAIL_SMS_ACK_FLAG = LOW;

char USER_PHONE_NUMBER[] = "+918349504911";

void __delay(uint32_t milliSec)
{
    uint32_t counter = 0;
    uint32_t cycles = 0;

    cycles = (milliSec * 48000) / 15;

    for (counter = 0; counter <= cycles; counter++) {
        // no operation
    }
}


/* Initialize LCD */
void LCD_Init()
{
    LCD_PortConfig();
    LCD_Initialize();
}


/* Draw initial layout on LCD */
void LCD_Draw_Init_Layout()
{
    /* Making the basic layout in LCD */
    lcdPrint("AMPHI ", 0, 0);
    lcdPrint(" WET  ", 0, 1);
    lcdPrint(" OFF  ", 0, 2);
    lcdPrint("|", 6, 0);
    lcdPrint(" MDC  ", 7, 0);
    lcdPrint(" WET  ", 7, 1);
    lcdPrint(" OFF  ", 7, 2);
    lcdPrint("|", 13, 0);
    lcdPrint(" MBACK", 14, 0);
    lcdPrint(" WET  ", 14, 1);
    lcdPrint(" OFF  ", 14, 2);
    lcdPrint("|", 6, 1);
    lcdPrint("|", 13, 1);
    lcdPrint("|", 6, 2);
    lcdPrint("|", 13, 2);
    lcdPrint("|", 6, 3);
    lcdPrint("|", 13, 3);
    lcdPrint("|", 6, 4);
    lcdPrint("|", 13, 4);
}


void Stop_Mode()
{
    //printf("Stop Mode Active\n");
    Pump_Switch(OFF);

    MDC_Solenoid_Switch(OFF);
    Amphi_Solenoid_Switch(OFF);
    MDC_Back_Solenoid_Switch(OFF);

    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    MDC_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);


    SOLENOID_IN_USE_FLAG_UPDATE(LOW);
}


void Automatic_Mode()
{
    PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    MDC_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(LOW);
    //printf("Automatic Mode Active\n");
    if((*(PTR_DEVICE_C_DATA + 1) == 'e') || (*(PTR_DEVICE_D_DATA + 1) == 'e') || (*(PTR_DEVICE_E_DATA + 1) == 'e')) {
        //printf("error in one of the sensor");
        Pump_Switch(OFF);

        MDC_Solenoid_Switch(OFF);
        //Amphi_Solenoid_Switch(OFF);
        MDC_Back_Solenoid_Switch(OFF);




        SOLENOID_IN_USE_FLAG_UPDATE(LOW);
    }

    else {

        if (*(PTR_DEVICE_A_DATA + 1) == '1' || *(PTR_DEVICE_B_DATA + 1) == '1') {
            AMPHI_DRY_NOTIFICATION_FLAG = HIGH;
            AMPHI_WET_NOTIFICATION_FLAG = LOW;
        }
        else {
            AMPHI_WET_NOTIFICATION_FLAG = HIGH;
            AMPHI_DRY_NOTIFICATION_FLAG = LOW;
        }

        if ((*(PTR_DEVICE_C_DATA + 1) == '1' || *(PTR_DEVICE_D_DATA + 1) == '1')) {
            lcdPrint(" DRY  ", 7, 1);
        }

        if ((*(PTR_DEVICE_C_DATA + 1) == '1' || *(PTR_DEVICE_D_DATA + 1) == '1') && SOLENOID_IN_USE_FLAG_STATUS() == LOW) {
            //printf("C is 1");
            //printf("MDC auto ON");
            MDC_Solenoid_Switch(ON);
            Amphi_Solenoid_Switch(OFF);
            MDC_Back_Solenoid_Switch(OFF);
            Pump_Switch(ON);
            SOLENOID_IN_USE_FLAG_UPDATE(HIGH);
        }

        if (((*(PTR_DEVICE_C_DATA + 1) == '0' && *(PTR_DEVICE_D_DATA + 1) == '0') || (*(PTR_DEVICE_C_DATA + 1) == 'e' && *(PTR_DEVICE_D_DATA + 1) == 'e')) && MDC_BACK_SOLENOID_STATUS() != ON) {
            //printf("in wrong if 1");
            Pump_Switch(OFF);
            MDC_Solenoid_Switch(OFF);
            SOLENOID_IN_USE_FLAG_UPDATE(LOW);
            lcdPrint(" WET  ", 7, 1);
        }

        if (*(PTR_DEVICE_E_DATA + 1) == '1') {
            lcdPrint(" DRY  ", 14, 1);
        }

        if (*(PTR_DEVICE_E_DATA + 1) == '1' && SOLENOID_IN_USE_FLAG_STATUS() == LOW) {
            MDC_Back_Solenoid_Switch(ON);
            MDC_Solenoid_Switch(OFF);
            Amphi_Solenoid_Switch(OFF);
            Pump_Switch(ON);
            SOLENOID_IN_USE_FLAG_UPDATE(HIGH);
        }

        if (((*(PTR_DEVICE_E_DATA + 1) == '0') || (*(PTR_DEVICE_E_DATA + 1) == 'e')) && MDC_SOLENOID_STATUS() != ON) {
            //printf("in wrong if 2");
            Pump_Switch(OFF);
            MDC_Back_Solenoid_Switch(OFF);
            SOLENOID_IN_USE_FLAG_UPDATE(LOW);
            lcdPrint(" WET  ", 14, 1);
        }

        if (SOLENOID_IN_USE_FLAG_STATUS() == LOW) {
            //printf("in wrong if 3");
            Pump_Switch(OFF);
        }
    }
}


void Semi_Automatic_Mode()
{

    //printf("Semi auto Mode Active\n");
    if (*(PTR_DEVICE_A_DATA + 1) == '1' || *(PTR_DEVICE_B_DATA + 1) == '1') {
        AMPHI_DRY_NOTIFICATION_FLAG = HIGH;
        AMPHI_WET_NOTIFICATION_FLAG = LOW;
    }
    else {
        AMPHI_WET_NOTIFICATION_FLAG = HIGH;
        AMPHI_DRY_NOTIFICATION_FLAG = LOW;
    }

    if (*(PTR_DEVICE_C_DATA + 1) == '1' || *(PTR_DEVICE_D_DATA + 1) == '1') {
        MDC_DRY_NOTIFICATION_FLAG = HIGH;
        MDC_WET_NOTIFICATION_FLAG = LOW;
    }
    else {
        MDC_WET_NOTIFICATION_FLAG = HIGH;
        MDC_DRY_NOTIFICATION_FLAG = LOW;
    }

    //printf("%c\n", *(PTR_DEVICE_E_DATA + 1));
    if (*(PTR_DEVICE_E_DATA + 1) == '1') {
        //printf("mdc back dry");
        MDC_BACK_DRY_NOTIFICATION_FLAG = HIGH;
        MDC_BACK_WET_NOTIFICATION_FLAG = LOW;
        //printf("%d\n", MDC_BACK_DRY_NOTIFICATION_FLAG);
    }
    else {
        //printf("mdc back wet");
        MDC_BACK_WET_NOTIFICATION_FLAG = HIGH;
        MDC_BACK_DRY_NOTIFICATION_FLAG = LOW;
    }
}


void Mode_Checker()
{
    //printf("In Mode Checker\n");
    if (STOP_MODE_FLAG_STATUS() == HIGH) {
        Stop_Mode();

        AUTOMATIC_MODE_FLAG_UPDATE(LOW);
        SEMI_AUTOMATIC_MODE_FLAG_UPDATE(LOW);
    }

    if (AUTOMATIC_MODE_FLAG_STATUS() == HIGH) {
        Automatic_Mode();

        STOP_MODE_FLAG_UPDATE(LOW);
        SEMI_AUTOMATIC_MODE_FLAG_UPDATE(LOW);
    }

    if (SEMI_AUTOMATIC_MODE_FLAG_STATUS() == HIGH) {
        Semi_Automatic_Mode();

        STOP_MODE_FLAG_UPDATE(LOW);
        AUTOMATIC_MODE_FLAG_UPDATE(LOW);
    }
}


void Data_Received_Checker()
{
    //printf("Data_Received_Checker\n");
    if (DEVICE_A_DATA_RECEIVED_FLAG_STATUS() == HIGH) {
        PTR_DEVICE_A_DATA = Device_A_Data();
        //printf("Data_A_Received\n");
        /* Printing Device A value in LCD */
        DEVICE_A_SENS_VALUE[0] = *(PTR_DEVICE_A_DATA + 3);
        DEVICE_A_SENS_VALUE[1] = *(PTR_DEVICE_A_DATA + 4);
        DEVICE_A_SENS_VALUE[2] = '\0';
        lcdPrint("A", 0, 3);
        lcdPrint(DEVICE_A_SENS_VALUE, 1, 3);
    }

    if (DEVICE_B_DATA_RECEIVED_FLAG_STATUS() == HIGH) {
        PTR_DEVICE_B_DATA = Device_B_Data();
        //printf("Data_B_Received\n");
        /* Printing Device B value in LCD */
        DEVICE_B_SENS_VALUE[0] = *(PTR_DEVICE_B_DATA + 3);
        DEVICE_B_SENS_VALUE[1] = *(PTR_DEVICE_B_DATA + 4);
        DEVICE_B_SENS_VALUE[2] = '\0';
        lcdPrint("B", 3, 3);
        lcdPrint(DEVICE_B_SENS_VALUE, 4, 3);
    }

    if (DEVICE_C_DATA_RECEIVED_FLAG_STATUS() == HIGH) {
        PTR_DEVICE_C_DATA = Device_C_Data();
        //printf("Data_C_Received\n");
        /* Printing Device C value in LCD */
        DEVICE_C_SENS_VALUE[0] = *(PTR_DEVICE_C_DATA + 3);
        DEVICE_C_SENS_VALUE[1] = *(PTR_DEVICE_C_DATA + 4);
        DEVICE_C_SENS_VALUE[2] = '\0';
        lcdPrint("C", 7, 3);
        lcdPrint(DEVICE_C_SENS_VALUE, 8, 3);
    }

    if (DEVICE_D_DATA_RECEIVED_FLAG_STATUS() == HIGH) {
        PTR_DEVICE_D_DATA = Device_D_Data();
        //printf("Data_D_Received\n");
        /* Printing Device D value in LCD */
        DEVICE_D_SENS_VALUE[0] = *(PTR_DEVICE_D_DATA + 3);
        DEVICE_D_SENS_VALUE[1] = *(PTR_DEVICE_D_DATA + 4);
        DEVICE_D_SENS_VALUE[2] = '\0';
        lcdPrint("D", 10, 3);
        lcdPrint(DEVICE_D_SENS_VALUE, 11, 3);
    }

    if (DEVICE_E_DATA_RECEIVED_FLAG_STATUS() == HIGH) {
        PTR_DEVICE_E_DATA = Device_E_Data();
        //printf("Data_E_Received\n");
        /* Printing Device E value in LCD */
        DEVICE_E_SENS_VALUE[0] = *(PTR_DEVICE_E_DATA + 3);
        DEVICE_E_SENS_VALUE[1] = *(PTR_DEVICE_E_DATA + 4);
        DEVICE_E_SENS_VALUE[2] = '\0';
        lcdPrint(" E", 14, 3);
        lcdPrint(DEVICE_E_SENS_VALUE, 16, 3);


        Sensor_Send_Counter = 0;  //A to E sensor value received so resetting the Sensor_Send_Counter
    }

    if ((DEVICE_A_DATA_RECEIVED_FLAG_STATUS() == HIGH) &&
        (DEVICE_B_DATA_RECEIVED_FLAG_STATUS() == HIGH) &&
        (DEVICE_C_DATA_RECEIVED_FLAG_STATUS() == HIGH) &&
        (DEVICE_D_DATA_RECEIVED_FLAG_STATUS() == HIGH) &&
        (DEVICE_E_DATA_RECEIVED_FLAG_STATUS() == HIGH) &&
        (MQTT_UPLOAD_PERMISSION_FLAG == HIGH)) {

        //printf("MQTT uploading\n");
        uint16_t index = 0;
        uint16_t deviceCounter = 0;

        DEVICE_A_DATA_RECEIVED_FLAG_UPDATE(LOW);
        DEVICE_B_DATA_RECEIVED_FLAG_UPDATE(LOW);
        DEVICE_C_DATA_RECEIVED_FLAG_UPDATE(LOW);
        DEVICE_D_DATA_RECEIVED_FLAG_UPDATE(LOW);
        DEVICE_E_DATA_RECEIVED_FLAG_UPDATE(LOW);

        for (index = 0; index <= 4 && deviceCounter <= 4; index++) {
            MQTT_PACKET[index] = *(PTR_DEVICE_A_DATA + deviceCounter);
            deviceCounter++;
        }

        deviceCounter = 0;

        for (index = 5; index <= 9 && deviceCounter <= 4; index++) {
            MQTT_PACKET[index] = *(PTR_DEVICE_B_DATA + deviceCounter);
            deviceCounter++;
        }

        deviceCounter = 0;

        for (index = 10; index <= 14 && deviceCounter <= 4; index++) {
            MQTT_PACKET[index] = *(PTR_DEVICE_C_DATA + deviceCounter);
            deviceCounter++;
        }

        deviceCounter = 0;

        for (index = 15; index <= 19 && deviceCounter <= 4; index++) {
            MQTT_PACKET[index] = *(PTR_DEVICE_D_DATA + deviceCounter);
            deviceCounter++;
        }

        deviceCounter = 0;

        for (index = 20; index <= 24 && deviceCounter <= 4; index++) {
            MQTT_PACKET[index] = *(PTR_DEVICE_E_DATA + deviceCounter);
            deviceCounter++;
        }

        deviceCounter = 0;

        MQTT_PACKET[25] = '\0';

        //MQTT_Publish();

        MQTT_UPLOAD_PERMISSION_FLAG = LOW;
    }
}


void Wet_Dry_Flag_Checker()
{
    //printf("Wet_Dry_Flag_Checker\n");
    if (AMPHI_DRY_NOTIFICATION_FLAG == HIGH) {
        if (AMPHI_DRY_SMS_ACK_FLAG == LOW) {
            lcdPrint(" DRY  ", 0, 1);
            SMS_Send(USER_PHONE_NUMBER, "ATTENTION: AMPHI DRY!!....Send *a1 to switch it ON");

            AMPHI_DRY_SMS_ACK_FLAG = HIGH;
            AMPHI_WET_SMS_ACK_FLAG = LOW;
        }
    }

    if (AMPHI_WET_NOTIFICATION_FLAG == HIGH) {
        if (AMPHI_WET_SMS_ACK_FLAG == LOW) {
            lcdPrint(" WET  ", 0, 1);
            SMS_Send(USER_PHONE_NUMBER, "ATTENTION: AMPHI WET!!....Send *a0 to switch it OFF");

            AMPHI_WET_SMS_ACK_FLAG = HIGH;
            AMPHI_DRY_SMS_ACK_FLAG = LOW;
        }
    }

    if (MDC_DRY_NOTIFICATION_FLAG == HIGH) {
        if (MDC_DRY_SMS_ACK_FLAG == LOW) {
            lcdPrint(" DRY  ", 7, 1);
            SMS_Send(USER_PHONE_NUMBER, "ATTENTION: MDC DRY!!....Send *m1 to switch it ON");

            MDC_DRY_SMS_ACK_FLAG = HIGH;
            MDC_WET_SMS_ACK_FLAG = LOW;
        }
    }

    if (MDC_WET_NOTIFICATION_FLAG == HIGH) {
        if (MDC_WET_SMS_ACK_FLAG == LOW) {
            lcdPrint(" WET  ", 7, 1);
            SMS_Send(USER_PHONE_NUMBER, "ATTENTION: MDC WET!!....Send *m0 to switch it OFF");

            MDC_WET_SMS_ACK_FLAG = HIGH;
            MDC_DRY_SMS_ACK_FLAG = LOW;
        }
    }

    if (MDC_BACK_DRY_NOTIFICATION_FLAG == HIGH) {
        if (MDC_BACK_DRY_SMS_ACK_FLAG == LOW) {
            //printf("mdc back dry sms sending");
            lcdPrint(" DRY  ", 14, 1);
            SMS_Send(USER_PHONE_NUMBER, "ATTENTION: MDC BACK DRY!!....Send *b1 to switch it ON");

            MDC_BACK_DRY_SMS_ACK_FLAG = HIGH;
            MDC_BACK_WET_SMS_ACK_FLAG = LOW;
        }
    }

    if (MDC_BACK_WET_NOTIFICATION_FLAG == HIGH) {
        if (MDC_BACK_WET_SMS_ACK_FLAG == LOW) {
            lcdPrint(" WET  ", 14, 1);
            SMS_Send(USER_PHONE_NUMBER, "ATTENTION: MDC BACK WET!!....Send *b0 to switch it OFF");

            MDC_BACK_WET_SMS_ACK_FLAG = HIGH;
            MDC_BACK_DRY_SMS_ACK_FLAG = LOW;
        }
    }
}


//void timer32_isr(void)
void T32_INT1_IRQHandler(void)
{
    MAP_Timer32_clearInterruptFlag(TIMER32_0_BASE);

    // do something
}


/* Interrupt handler for sensor poll timer */
void TA0_N_IRQHandler(void)
{
    MAP_Timer_A_clearInterruptFlag(TIMER_A0_BASE);
    //printf("sense poll timer interrupt handler\n");
    if (Sensor_Send_Counter == 0) {
        //printf("LoRa A send");
        sensPoll_TimerA0_Stop();
        LoRa_Send('A');
        loraResponseWait_TimerA1_Start();

    }

    if (Sensor_Send_Counter == 1) {
        //printf("LoRa B send");
        sensPoll_TimerA0_Stop();
        LoRa_Send('B');
        loraResponseWait_TimerA1_Start();
    }

    if (Sensor_Send_Counter == 2) {
        //printf("LoRa C send");
        sensPoll_TimerA0_Stop();
        LoRa_Send('C');
        loraResponseWait_TimerA1_Start();
    }

    if (Sensor_Send_Counter == 3) {
        //printf("LoRa D send");
        sensPoll_TimerA0_Stop();
        LoRa_Send('D');
        loraResponseWait_TimerA1_Start();
    }

    if (Sensor_Send_Counter == 4) {
        //printf("LoRa E send");
        sensPoll_TimerA0_Stop();
        LoRa_Send('E');
        loraResponseWait_TimerA1_Start();
    }

    Sensor_Send_Counter++;

    if (Sensor_Send_Counter == 5) {
        //Sensor_Send_Counter = 0;
        MQTT_UPLOAD_PERMISSION_FLAG = HIGH;
    }

    if (Sensor_Send_Counter > 6) {
        Sensor_Send_Counter = 0;
    }
}


/* Interrupt handler for LoRa response wait timer */
void TA1_N_IRQHandler(void)
{
    MAP_Timer_A_clearInterruptFlag(TIMER_A1_BASE);

    //printf("LoRa response timer interrupt handler\n");
    if (Sensor_Send_Counter == 1) {
        //printf("LoRa A error");
        LoRa_Error('A');

        loraResponseWait_TimerA1_Stop();
        sensPoll_TimerA0_Init();
        loraResponseWait_TimerA1_Init();
        sensorPoll_TimerA0_Start();
    }

    if (Sensor_Send_Counter == 2) {
        //printf("LoRa B error");
        LoRa_Error('B');

        loraResponseWait_TimerA1_Stop();
        sensPoll_TimerA0_Init();
        loraResponseWait_TimerA1_Init();
        sensorPoll_TimerA0_Start();
    }

    if (Sensor_Send_Counter == 3) {
        //printf("LoRa C error");
        LoRa_Error('C');

        loraResponseWait_TimerA1_Stop();
        sensPoll_TimerA0_Init();
        loraResponseWait_TimerA1_Init();
        sensorPoll_TimerA0_Start();
    }

    if (Sensor_Send_Counter == 4) {
        //printf("LoRa D error");
        LoRa_Error('D');

        loraResponseWait_TimerA1_Stop();
        sensPoll_TimerA0_Init();
        loraResponseWait_TimerA1_Init();
        sensorPoll_TimerA0_Start();
    }

    if (Sensor_Send_Counter == 5) {
        //printf("LoRa E error");
        LoRa_Error('E');

        loraResponseWait_TimerA1_Stop();
        sensPoll_TimerA0_Init();
        loraResponseWait_TimerA1_Init();
        sensorPoll_TimerA0_Start();
    }
}


int main(void)
{
    /* Stop Watchdog */
    MAP_WDT_A_holdTimer();

    // Enabling the FPU for floating point operation
    MAP_FPU_enableModule();
    MAP_FPU_enableLazyStacking();

    /* Setting Flash wait state */
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);


    /* Setting DCO to 48MHz */
    MAP_PCM_setPowerState(PCM_AM_LDO_VCORE1);
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_48);

    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    /* wait for the module to initialize */
    uint32_t indConfgMClk;
    for(indConfgMClk=0;indConfgMClk<1000;indConfgMClk++);

    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_2);

    /* wait for the module to initialize */
    uint32_t indConfgSMClk;
    for(indConfgSMClk=0;indConfgSMClk<1000;indConfgSMClk++);


    /* Starting and enabling ACLK (32kHz) */
    MAP_CS_setReferenceOscillatorFrequency(CS_REFO_128KHZ);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_4);

    /* wait for the module to initialize */
    uint32_t indConfgAclkClk;
    for(indConfgAclkClk=0;indConfgAclkClk<1000;indConfgAclkClk++);

    //printf("%d\n", CS_getMCLK());
    //printf("%d\n", CS_getSMCLK());
    //printf("%d\n", CS_getACLK());

    /* Initializing ports */
    Ports_Init();
    __delay(2000);
    Ports_Init();


    /* Initializing LCD*/
    LCD_Init();

    __delay(2000);

    /* Initializing LCD*/
    LCD_Init();

    lcdPrint("Initializing GSM....", 0, 0);
    GSM_Init();

    GSM_Init();

    lcdPrint("Initializing LoRa...", 0, 0);
    __delay(2000); // 2000ms delay
    LoRa_Init();

    /* Initializing timer configurations */
    //timerInit();
    sensPoll_TimerA0_Init();
    loraResponseWait_TimerA1_Init();

    /* Initializing ADC14 for cell voltage calculation */
    ADC_Init();

    //__no_operation();

    /* Enabling interrupts */
    MAP_ADC14_enableInterrupt(ADC_INT0);
    MAP_Interrupt_enableInterrupt(INT_ADC14);

    MAP_Interrupt_enableInterrupt(INT_TA0_N);
    MAP_Interrupt_enableInterrupt(INT_TA1_N);

    MAP_Interrupt_enableInterrupt(INT_T32_INT1);

    MAP_Interrupt_enableMaster();

    /* Configuring interrupt priorities  */
    MAP_Interrupt_setPriority(INT_EUSCIA0, 0x00);
    MAP_Interrupt_setPriority(INT_EUSCIA2, 0x20);
    MAP_Interrupt_setPriority(INT_TA1_N, 0x40);
    MAP_Interrupt_setPriority(INT_TA0_N, 0x60);


    lcdPrint("GSM setup test......", 0, 0);
    lcdPrint("Wait about 40 sec...", 0, 1);
    __delay(30000); // 30000ms delay for GSM module initialization

    GSM_Setup();
    //MQTT_Init();

    lcdPrint("GSM test done ......", 0, 0);
    lcdPrint("                    ", 0, 1);
    __delay(2000); // 2000ms delay
    lcdPrint("Device Ready!!!.....", 0, 0);

    LCD_Draw_Init_Layout();

    //cellVoltage();
    //printf(cellVoltage());

    /* Initializing timer to poll for sensor data */
    //sensePollTimer32(12800);
    sensorPoll_TimerA0_Start();

    SOLENOID_IN_USE_FLAG_UPDATE(LOW);

    while(1)
    {
        //MAP_PCM_gotoLPM0();

        GSM_Status_Checker();

        if (powerSupply() == 1) {

            if (POWER_SUPPLY_AVAIL_SMS_ACK_FLAG == LOW) {
                SMS_Send(USER_PHONE_NUMBER, "Power Supply Available Now");
                POWER_SUPPLY_NOT_AVAIL_SMS_ACK_FLAG = LOW;
                POWER_SUPPLY_AVAIL_SMS_ACK_FLAG = HIGH;
            }
            //printf("power supply available\n");

            GSM_URC_Read();

            SMS_Read();

            SMS_Delete();

            LoRa_Response_Read();

            Semi_Automatic_Mode_Control_Check();

            Pump_Status_Checker();

            Solenoid_Status_Checker();

            Wet_Dry_Flag_Checker();

            Data_Received_Checker();

            Mode_Checker();
        }
        else {
            //do something
            printf("power supply not available\n");
            if (POWER_SUPPLY_NOT_AVAIL_SMS_ACK_FLAG == LOW) {
                SMS_Send(USER_PHONE_NUMBER, "No Power Supply");
                POWER_SUPPLY_NOT_AVAIL_SMS_ACK_FLAG = HIGH;
                POWER_SUPPLY_AVAIL_SMS_ACK_FLAG = LOW;
            }
        }
    }
}

/*
 * lora.h
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */

#ifndef LORA_H_
#define LORA_H_

#include <stdint.h>
#include <stdbool.h>


void LoRa_Init();
void LoRa_Send(char deviceID);
void LoRa_Error(char deviceID);
char* LoRa_Device_Buffer(char deviceID);
char* Device_A_Data(void);
char* Device_B_Data(void);
char* Device_C_Data(void);
char* Device_D_Data(void);
char* Device_E_Data(void);
uint8_t DEVICE_A_DATA_RECEIVED_FLAG_STATUS(void);
uint8_t DEVICE_B_DATA_RECEIVED_FLAG_STATUS(void);
uint8_t DEVICE_C_DATA_RECEIVED_FLAG_STATUS(void);
uint8_t DEVICE_D_DATA_RECEIVED_FLAG_STATUS(void);
uint8_t DEVICE_E_DATA_RECEIVED_FLAG_STATUS(void);
void DEVICE_A_DATA_RECEIVED_FLAG_UPDATE(uint8_t command);
void DEVICE_B_DATA_RECEIVED_FLAG_UPDATE(uint8_t command);
void DEVICE_C_DATA_RECEIVED_FLAG_UPDATE(uint8_t command);
void DEVICE_D_DATA_RECEIVED_FLAG_UPDATE(uint8_t command);
void DEVICE_E_DATA_RECEIVED_FLAG_UPDATE(uint8_t command);
void LoRa_Response_Read(void);

#endif /* LORA_H_ */

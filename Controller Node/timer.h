/*
 * timer.h
 *
 *  Created on: Aug 8, 2018
 *      Author: Aaditya Chaudhary
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include <stdbool.h>

void timerInit(void);
void sensePollTimer32(uint32_t clockTicks);
void sensPoll_TimerA0_Init(void);
void sensorPoll_TimerA0_Start(void);
void sensPoll_TimerA0_Stop(void);
void loraResponseWait_TimerA1_Init(void);
void loraResponseWait_TimerA1_Start(void);
void loraResponseWait_TimerA1_Stop(void);

#endif /* TIMER_H_ */

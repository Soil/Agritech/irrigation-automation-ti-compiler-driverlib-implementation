/*
 * Controller.h
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#define PUMP_ON                                      (0x01)
#define PUMP_OFF                                     (0x00)
#define MDC_SOLENOID_ON                              (0x01)
#define MDC_SOLENOID_OFF                             (0x00)
#define MDC_BACK_SOLENOID_ON                         (0x01)
#define MDC_BACK_SOLENOID_OFF                        (0x00)
#define AMPHI_SOLENOID_ON                            (0x01)
#define AMPHI_SOLENOID_OFF                           (0x00)

#include <stdint.h>
#include <stdbool.h>

bool Pump_Switch(uint8_t command);
bool MDC_Solenoid_Switch(uint8_t command);
bool MDC_Back_Solenoid_Switch(uint8_t command);
bool Amphi_Solenoid_Switch(uint8_t command);
uint8_t MDC_SOLENOID_STATUS(void);
uint8_t MDC_BACK_SOLENOID_STATUS(void);
void Pump_Status_Checker(void);
void Solenoid_Status_Checker(void);
void SOLENOID_IN_USE_FLAG_UPDATE(uint8_t command);
uint8_t SOLENOID_IN_USE_FLAG_STATUS(void);
void PUMP_SWITCH_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command);
void AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command);
void MDC_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command);
void MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_UPDATE(uint8_t command);
uint8_t PUMP_SWITCH_MANUAL_TOGGLE_FLAG_STATUS(void);
uint8_t AMPHI_SOLENOID_MANUAL_TOGGLE_FLAG_STATUS(void);
uint8_t MDC_SOLENOID_MANUAL_TOGGLE_FLAG_STATUS(void);
uint8_t MDC_BACK_SOLENOID_MANUAL_TOGGLE_FLAG_STATUS(void);
void Semi_Automatic_Mode_Control_Check(void);


#endif /* CONTROLLER_H_ */

/*****************************************************************/
/*********                                               *********/
/********* AUTOR:    ANTONIO ARMANDO BIZA FERREIRA       *********/
/********* PROJECTO: ECG                                 *********/
/********* DATA:     01-02-2017                          *********/
/********* IDE:      CODE COMPOSER STUDIO V5             *********/
/********* MCU:      MSP432                              *********/
/*********                                               *********/
/********* FILE:     LCD.H                               *********/
/*********                                               *********/
/*****************************************************************/


/*****************************************************************/
/********* INCLUDE                                       *********/
/*****************************************************************/
#include "lcd.h"
#include <msp432.h>

/*****************************************************************/
/********* DEFINES                                       *********/
/*****************************************************************/

#define EN                      (PORT3 + BIT5)
#define RS                      (PORT5 + BIT1)

#define D4                      (PORT4 + BIT7)
#define D5                      (PORT4 + BIT5)
#define D6                      (PORT4 + BIT0)
#define D7                      (PORT6 + BIT1)

/*COMMAND*/
#define CLEAR                   0x01

/*****************************************************************/
/********* LOCAL FUNCTIONS                               *********/
/*****************************************************************/

/*LCD PIN OUT DIRECTION*/
void lcdDirPinOut(unsigned int pin){

    if((pin & 0xFF00) == PORT3)
    {
        P3DIR |= (pin & 0x00FF);

    }else if((pin & 0xFF00) == PORT5){

        P5DIR |= (pin & 0x00FF);

    }else if((pin & 0xFF00) == PORT6){

        P6DIR |= (pin & 0x00FF);
    }
    else{

        P4DIR |= (pin & 0x00FF);

    }

}

/*LCD PIN OUT SET*/
void lcdSetPinOut(unsigned int pin){

    if((pin & 0xFF00) == PORT3){

        P3OUT |= (pin & 0x00FF);

    }else if((pin & 0xFF00) == PORT5){

        P5OUT |= (pin & 0x00FF);

    }else if((pin & 0xFF00) == PORT6){

        P6OUT |= (pin & 0x00FF);
    }
    else{

        P4OUT |= (pin & 0x00FF);

    }

}


/*LCD PIN OUT CLR*/
void lcdClrPinOut(unsigned int pin){

    if((pin & 0xFF00) == PORT3){

        P3OUT &= ~(pin & 0x00FF);

    }else if((pin & 0xFF00) == PORT5){

        P5OUT &= ~(pin & 0x00FF);

    }else if((pin & 0xFF00) == PORT6){

        P6OUT &= ~(pin & 0x00FF);
    }
    else{
        P4OUT &= ~(pin & 0x00FF);

    }

}

void lcdSetValue(unsigned char value){

     if(value & 0x08){
            lcdSetPinOut(D7);
        }else{
            lcdClrPinOut(D7);
        }

        if(value & 0x04){
            lcdSetPinOut(D6);
        }else{
            lcdClrPinOut(D6);
        }

        if(value & 0x02){
            lcdSetPinOut(D5);
        }else{
            lcdClrPinOut(D5);
        }

        if(value & 0x01){
            lcdSetPinOut(D4);
        }else{
            lcdClrPinOut(D4);
        }
        delay_us(5);
}

void lcdTriggerEN(){

    lcdSetPinOut(EN);
    delay_us(5);
    lcdClrPinOut(EN);
  // delay_us(5);
}

void lcdWriteData(unsigned char data){
    lcdSetPinOut(RS);           // Set RS to Data
    lcdSetValue(data >> 4);     // Upper nibble
    lcdTriggerEN();
    lcdSetValue(data);          // Lower nibble
    lcdTriggerEN();
    delay_us(30);               // Delay > 50 us
}

void lcdWriteCmd(unsigned char cmd){
    lcdClrPinOut(RS);           // Set RS to Cmd
    lcdSetValue(cmd >> 4);      // Upper nibble
    lcdTriggerEN();
    lcdSetValue(cmd);           // Lower nibble
    lcdTriggerEN();
    delay_ms(2);                // Delay > 5ms
}




/*****************************************************************/
/********* GLOBAL FUNCTIONS                              *********/
/*****************************************************************/

/*
 *
 */
void LCD_PortConfig(void)
{
    // Direction
    lcdDirPinOut(D4);
    lcdDirPinOut(D5);
    lcdDirPinOut(D6);
    lcdDirPinOut(D7);
    lcdDirPinOut(EN);
    lcdDirPinOut(RS);
}


/*
 *
 */
void LCD_Initialize(void)
{

   /* delay_us(80);
    lcdWriteCmd(0x28); // 4-bit, 2 line, 5x8
    P2OUT = 0x03;      // Start LCD (send 0x03)
    lcdTriggerEN();    // Send 0x03 3 times at 5ms then 100 us
    delay_ms(2);
    lcdTriggerEN();
    delay_ms(2);
    lcdTriggerEN();
    delay_ms(2);

    lcdWriteCmd(0x28); // 4-bit, 2 line, 5x8
    P2OUT = 0x02;      // Switch to 4-bit mode
    lcdTriggerEN();
    delay_ms(2);*/


    lcdWriteCmd(0x28); // 4-bit, 2 line, 5x8
    lcdWriteCmd(0x08); // Instruction Flow
    lcdWriteCmd(0x01); // Clear LCD
    lcdWriteCmd(0x06); // Auto-Increment
    lcdWriteCmd(0x0C); // Display On, No blink
}


void LCD_SetText(char* text, int x, int y){
    unsigned int i;

    if (x < 41) {
        x |= 0x80;      // Set LCD for first line write
        switch (y){
        case 1:
            x |= 0x40;  // Set LCD for second line write
            break;
        case 2:
            x |= 0x60;  // Set LCD for first line write reverse
            break;
        case 3:
            x |= 0x20;  // Set LCD for second line write reverse
            break;
        }
        lcdWriteCmd(x);
    }

    i = 0;
    while (text[i] != '\0') {
        lcdWriteData(text[i]);
        i++;
    }
}

void LCD_SetInt(int val, int x, int y)
{
    char number_string[16];

    sprintf(number_string, "%d", val); // Convert the integer to character string
    LCD_SetText(number_string, x, y);
}

void LCD_Clear()
{
    lcdWriteCmd(CLEAR);
}

void LCD_LoadSymbols(void)
{
    int j;
    static char empty[]   = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    static char heart[]   = {0x00, 0x0A, 0x1F, 0x1F, 0x1F, 0x0E, 0x04, 0x00};
    static char bat_100[] = {0x0E, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F};
    static char bat_75[]  = {0x0E, 0x1F, 0x11, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F};
    static char bat_50[]  = {0x0E, 0x1F, 0x11, 0x11, 0x11, 0x1F, 0x1F, 0x1F};
    static char bat_25[]  = {0x0E, 0x1F, 0x11, 0x11, 0x11, 0x11, 0x1F, 0x1F};
    static char bat_0[]   = {0x0E, 0x1F, 0x11, 0x11, 0x11, 0x11, 0x11, 0x1F};

    lcdWriteCmd(0x40);


    for(j=0;j!=8;j++){
        lcdWriteData(empty[j]);
    }

    for(j=0;j!=8;j++){
        lcdWriteData(heart[j]);
    }

    for(j=0;j!=8;j++){
        lcdWriteData(bat_100[j]);
    }

    for(j=0;j!=8;j++){
        lcdWriteData(bat_75[j]);
    }
    for(j=0;j!=8;j++){
        lcdWriteData(bat_50[j]);
    }
    for(j=0;j!=8;j++){
        lcdWriteData(bat_25[j]);
    }
    for(j=0;j!=8;j++){
        lcdWriteData(bat_0[j]);
    }

    lcdWriteCmd(CMD_END);
}

void LCD_SetSymbol(unsigned char symbol, unsigned char offset, unsigned char line)
{
    lcdWriteCmd(line+offset);
    lcdWriteData(symbol);
}

void lcdPrint(char* text, int x, int y)
{
    if(y==0) {
        LCD_SetText(text, x, 0);
    }

    if(y==1) {
        LCD_SetText(text, x, 1);
    }

    if(y==2) {
        LCD_SetText(text, 20+x, 0);
    }

    if(y==3) {
        LCD_SetText(text, 20+x, 1);
    }
}

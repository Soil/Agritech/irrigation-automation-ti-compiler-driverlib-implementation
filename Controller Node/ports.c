/* ports.c  */

#include "ports.h"
//#include "driverlib.h"
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

void Ports_Init(void)
{
    terminatePorts();
    Interrupt_disableMaster();

    /* P3.6 --> Input pin for phase detector(power detector pin) */
    MAP_GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P3, GPIO_PIN6);
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P3, GPIO_PIN6);

    /* P6.0(A15) --> Configuring GPIO (6.0 A15) for measuring ADC reading of cell voltage */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P6, GPIO_PIN6,GPIO_TERTIARY_MODULE_FUNCTION);

    /* P3.2(Rx) & P3.3(Tx) --> Selecting P3.2(Rx) and P3.3(Tx) in UART mode for GSM module */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P3, GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

    /* P1.2(Rx) & P1.3(Tx) --> Selecting P1.2(Rx) and P1.3(Tx) in UART mode for LoRa module */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

    /* P5.6 --> Input pin for GSM status */
    MAP_GPIO_setAsInputPin(GPIO_PORT_P5, GPIO_PIN6);
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P5, GPIO_PIN6);

    /* P2.7 --> Output pin for GSM power key and initialize it as LOW */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN7);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN7);

    /* Output pins for relay ckt and initializing them as LOW */
    /* P5.2 --> Pump ON relay */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN2);
    MAP_GPIO_setDriveStrengthHigh(GPIO_PORT_P5, GPIO_PIN2);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN2);

    /* 5.0 --> Pump OFF relay */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0);
    MAP_GPIO_setDriveStrengthHigh(GPIO_PORT_P5, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0);

    /* P1.7 --> Amphitheater relay */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN7);
    MAP_GPIO_setDriveStrengthHigh(GPIO_PORT_P1, GPIO_PIN7);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN7);

    /* P1.6 --> MDC relay */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN6);
    MAP_GPIO_setDriveStrengthHigh(GPIO_PORT_P1, GPIO_PIN6);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN6);

    /* P5.7 --> MDC Back relay */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN7);
    MAP_GPIO_setDriveStrengthHigh(GPIO_PORT_P5, GPIO_PIN7);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN7);

    /* P3.0 --> Empty relay1 */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0);

    /* P2.5 --> Empty relay2 */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN5);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN5);

    /* Configuring pins for Ultrasonic sensor */
    /* P6.7 --> Echo Pin */
    MAP_GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P6, GPIO_PIN7);

    /* P3.7 --> Trigger Pin */
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN7);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN7);
}


void terminatePorts(void)
{
    P1DIR = 0x00;
    P2DIR = 0x00;
    P3DIR = 0x00;
    P4DIR = 0x00;
    P5DIR = 0x00;
    P6DIR = 0x00;
    P7DIR = 0x00;
    P8DIR = 0x00;
    P9DIR = 0x00;
    P10DIR = 0x00;
    P1REN = 0xff;
    P2REN = 0xff;
    P3REN = 0xff;
    P4REN = 0xff;
    P5REN = 0xff;
    P6REN = 0xff;
    P7REN = 0xff;

    PSS_setHighSidePerformanceMode(PSS_NORMAL_PERFORMANCE_MODE);
    //PSS_setLowSidePerformanceMode(PSS_NORMAL_PERFORMANCE_MODE);
    PCM_enableRudeMode();
}

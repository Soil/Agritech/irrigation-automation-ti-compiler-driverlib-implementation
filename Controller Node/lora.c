/*
 * lora.c
 *
 *  Created on: Jul 10, 2018
 *      Author: Aaditya Chaudhary
 */

#include <stdint.h>
#include <stdbool.h>
#include <lora.h>
#include <timer.h>

#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

#define HIGH    1
#define LOW     0

// circular buffer constants declarations
#define LORA_EUART_BUFFER_SIZE 512

volatile char lora_rx_char;
volatile char lora_euart_rx_buffer[LORA_EUART_BUFFER_SIZE];        // the actual buffer, now 2048 bytes long
volatile uint16_t lora_euart_rx_buffer_rd_ptr = 0;            // the current read position
volatile uint16_t lora_euart_rx_buffer_wr_ptr = 0;            // the current write position


volatile uint8_t device_A_buffer_wr_ptr = 0;
volatile uint8_t device_B_buffer_wr_ptr = 0;
volatile uint8_t device_C_buffer_wr_ptr = 0;
volatile uint8_t device_D_buffer_wr_ptr = 0;
volatile uint8_t device_E_buffer_wr_ptr = 0;

uint8_t DEVICE_A_DATA_RECEIVED_FLAG = LOW;
uint8_t DEVICE_B_DATA_RECEIVED_FLAG = LOW;
uint8_t DEVICE_C_DATA_RECEIVED_FLAG = LOW;
uint8_t DEVICE_D_DATA_RECEIVED_FLAG = LOW;
uint8_t DEVICE_E_DATA_RECEIVED_FLAG = LOW;

static char Device_A_buffer[5];
static char Device_B_buffer[5];
static char Device_C_buffer[5];
static char Device_D_buffer[5];
static char Device_E_buffer[5];

volatile uint8_t DEVICE_A_RECEIVING_FLAG = LOW;
volatile uint8_t DEVICE_B_RECEIVING_FLAG = LOW;
volatile uint8_t DEVICE_C_RECEIVING_FLAG = LOW;
volatile uint8_t DEVICE_D_RECEIVING_FLAG = LOW;
volatile uint8_t DEVICE_E_RECEIVING_FLAG = LOW;


// Baud rate set to 9600
const eUSCI_UART_Config uartConfigLora =
{
 EUSCI_A_UART_CLOCKSOURCE_SMCLK,                 // SMCLK Clock Source
 156,                                            // BRDIV = 156
 4,                                              // UCxBRF = 4
 0,                                              // UCxBRS = 0
 EUSCI_A_UART_NO_PARITY,                         // No Parity
 EUSCI_A_UART_LSB_FIRST,                         // LSB First
 EUSCI_A_UART_ONE_STOP_BIT,                      // One stop bit
 EUSCI_A_UART_MODE,                              // UART mode
 EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION   // Oversampling ON
};


void uartPortInit(void)
{
    /* Configuring UART Module */
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfigLora);

    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A0_BASE);

    /* Enabling interrupts on receiving data */
    MAP_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    MAP_Interrupt_enableInterrupt(INT_EUSCIA0);
}


void LoRa_Init()
{
    uartPortInit();
}


void LoRa_Send(char deviceID)
{
    MAP_UART_transmitData(EUSCI_A0_BASE, deviceID);
}


void LoRa_Error(char deviceID)
{
    uint8_t index;

    if (deviceID == 'A') {
        for (index = 0; index < 5; index++) {
            Device_A_buffer[index] = 'e';
        }
        DEVICE_A_DATA_RECEIVED_FLAG = HIGH;
    }

    if (deviceID == 'B') {
        for (index = 0; index < 5; index++) {
            Device_B_buffer[index] = 'e';
        }
        DEVICE_B_DATA_RECEIVED_FLAG = HIGH;
    }

    if (deviceID == 'C') {
        for (index = 0; index < 5; index++) {
            Device_C_buffer[index] = 'e';
        }
        DEVICE_C_DATA_RECEIVED_FLAG = HIGH;
    }

    if (deviceID == 'D') {
        for (index = 0; index < 5; index++) {
            Device_D_buffer[index] = 'e';
        }
        DEVICE_D_DATA_RECEIVED_FLAG = HIGH;
    }

    if (deviceID == 'E') {
        for (index = 0; index < 5; index++) {
            Device_E_buffer[index] = 'e';
        }
        DEVICE_E_DATA_RECEIVED_FLAG = HIGH;
    }

}


char* LoRa_Device_Buffer(char deviceID)
{
    uint8_t index;
    char Device_buffer[6];

    if (deviceID == 'A') {
        for (index = 0; index < 5; index++) {
            Device_buffer[index] = Device_A_buffer[index];
        }
    }

    if (deviceID == 'B') {
        for (index = 0; index < 5; index++) {
            Device_buffer[index] = Device_B_buffer[index];
        }
    }

    if (deviceID == 'C') {
        for (index = 0; index < 5; index++) {
            Device_buffer[index] = Device_C_buffer[index];
        }
    }

    if (deviceID == 'D') {
        for (index = 0; index < 5; index++) {
            Device_buffer[index] = Device_D_buffer[index];
        }
    }

    if (deviceID == 'E') {
        for (index = 0; index < 5; index++) {
            Device_buffer[index] = Device_E_buffer[index];
        }
    }

    Device_buffer[5] = '\0';

    return Device_buffer;
}


char* Device_A_Data(void) {
    return Device_A_buffer;
}


char* Device_B_Data(void) {
    return Device_B_buffer;
}


char* Device_C_Data(void) {
    return Device_C_buffer;
}


char* Device_D_Data(void) {
    return Device_D_buffer;
}


char* Device_E_Data(void) {
    return Device_E_buffer;
}


uint8_t DEVICE_A_DATA_RECEIVED_FLAG_STATUS(void)
{
    return DEVICE_A_DATA_RECEIVED_FLAG;
}


uint8_t DEVICE_B_DATA_RECEIVED_FLAG_STATUS(void)
{
    return DEVICE_B_DATA_RECEIVED_FLAG;
}


uint8_t DEVICE_C_DATA_RECEIVED_FLAG_STATUS(void)
{
    return DEVICE_C_DATA_RECEIVED_FLAG;
}


uint8_t DEVICE_D_DATA_RECEIVED_FLAG_STATUS(void)
{
    return DEVICE_D_DATA_RECEIVED_FLAG;
}


uint8_t DEVICE_E_DATA_RECEIVED_FLAG_STATUS(void)
{
    return DEVICE_E_DATA_RECEIVED_FLAG;
}


void DEVICE_A_DATA_RECEIVED_FLAG_UPDATE(uint8_t command)
{
    DEVICE_A_DATA_RECEIVED_FLAG = command;
}


void DEVICE_B_DATA_RECEIVED_FLAG_UPDATE(uint8_t command)
{
    DEVICE_B_DATA_RECEIVED_FLAG = command;
}


void DEVICE_C_DATA_RECEIVED_FLAG_UPDATE(uint8_t command)
{
    DEVICE_C_DATA_RECEIVED_FLAG = command;
}


void DEVICE_D_DATA_RECEIVED_FLAG_UPDATE(uint8_t command)
{
    DEVICE_D_DATA_RECEIVED_FLAG  = command;
}


void DEVICE_E_DATA_RECEIVED_FLAG_UPDATE(uint8_t command)
{
    DEVICE_E_DATA_RECEIVED_FLAG = command;
}

/* EUSCI A0 UART ISR */
void EUSCIA0_IRQHandler(void)
{
    uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A0_BASE);

    //printf("In LoRa uart INT handler \n");
    MAP_UART_clearInterruptFlag(EUSCI_A0_BASE, status);

    if(status & EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG)
    {

        //circular buffer implementation
        lora_euart_rx_buffer[lora_euart_rx_buffer_wr_ptr++] = MAP_UART_receiveData(EUSCI_A0_BASE);  // Store the received data

        if (lora_euart_rx_buffer_wr_ptr >= LORA_EUART_BUFFER_SIZE) {    // Increment write pointer
            lora_euart_rx_buffer_wr_ptr = 0;
        }

        MAP_Timer_A_stopTimer(TIMER_A1_BASE);       //loraResponseWait_TimerA1_Stop
        //MAP_Timer_A_clearTimer(TIMER_A1_BASE);
        //loraResponseWait_TimerA1_Stop();
    }
}


void LoRa_Response_Read(void)
{
    while (lora_euart_rx_buffer_rd_ptr != lora_euart_rx_buffer_wr_ptr) {   // While there's data in the buffer
        lora_rx_char  = lora_euart_rx_buffer[lora_euart_rx_buffer_rd_ptr++];

        if (lora_rx_char == 'A') {
            DEVICE_A_RECEIVING_FLAG = HIGH;
            //printf("A received\n");
            //loraResponseWait_TimerA1_Stop();
        }

        if (lora_rx_char == 'B') {
            DEVICE_B_RECEIVING_FLAG = HIGH;
            //printf("B received\n");
            //loraResponseWait_TimerA1_Stop();
        }

        if (lora_rx_char == 'C') {
            DEVICE_C_RECEIVING_FLAG = HIGH;
            //printf("C received\n");
            //loraResponseWait_TimerA1_Stop();
        }

        if (lora_rx_char == 'D') {
            DEVICE_D_RECEIVING_FLAG = HIGH;
            //printf("D received\n");
            //loraResponseWait_TimerA1_Stop();
        }

        if (lora_rx_char == 'E') {
            DEVICE_E_RECEIVING_FLAG = HIGH;
            //printf("E received\n");
            //loraResponseWait_TimerA1_Stop();
        }


        if (DEVICE_A_RECEIVING_FLAG == HIGH) {

            Device_A_buffer[device_A_buffer_wr_ptr++] = lora_rx_char;

            if (device_A_buffer_wr_ptr == 5) {

                device_A_buffer_wr_ptr = 0;
                DEVICE_A_RECEIVING_FLAG = LOW;
                DEVICE_A_DATA_RECEIVED_FLAG = HIGH;

                sensPoll_TimerA0_Init();
                loraResponseWait_TimerA1_Init();
                sensorPoll_TimerA0_Start();
            }
        }

        if (DEVICE_B_RECEIVING_FLAG == HIGH) {

            Device_B_buffer[device_B_buffer_wr_ptr++] = lora_rx_char;

            if (device_B_buffer_wr_ptr == 5) {

                device_B_buffer_wr_ptr = 0;
                DEVICE_B_RECEIVING_FLAG = LOW;
                DEVICE_B_DATA_RECEIVED_FLAG = HIGH;

                sensPoll_TimerA0_Init();
                loraResponseWait_TimerA1_Init();
                sensorPoll_TimerA0_Start();
            }
        }

        if (DEVICE_C_RECEIVING_FLAG == HIGH) {

            Device_C_buffer[device_C_buffer_wr_ptr++] = lora_rx_char;

            if (device_C_buffer_wr_ptr == 5) {

                device_C_buffer_wr_ptr = 0;
                DEVICE_C_RECEIVING_FLAG = LOW;
                DEVICE_C_DATA_RECEIVED_FLAG = HIGH;

                sensPoll_TimerA0_Init();
                loraResponseWait_TimerA1_Init();
                sensorPoll_TimerA0_Start();
            }
        }

        if (DEVICE_D_RECEIVING_FLAG == HIGH) {

            Device_D_buffer[device_D_buffer_wr_ptr++] = lora_rx_char;

            if (device_D_buffer_wr_ptr == 5) {

                device_D_buffer_wr_ptr = 0;
                DEVICE_D_RECEIVING_FLAG = LOW;
                DEVICE_D_DATA_RECEIVED_FLAG = HIGH;

                sensPoll_TimerA0_Init();
                loraResponseWait_TimerA1_Init();
                sensorPoll_TimerA0_Start();
            }
        }

        if (DEVICE_E_RECEIVING_FLAG == HIGH) {

            Device_E_buffer[device_E_buffer_wr_ptr++] = lora_rx_char;

            if (device_E_buffer_wr_ptr == 5) {

                device_E_buffer_wr_ptr = 0;
                DEVICE_E_RECEIVING_FLAG = LOW;
                DEVICE_E_DATA_RECEIVED_FLAG = HIGH;

                /*
                  DEVICE_A_DATA_RECEIVED_FLAG = LOW;
                  DEVICE_B_DATA_RECEIVED_FLAG = LOW;
                  DEVICE_C_DATA_RECEIVED_FLAG = LOW;
                  DEVICE_D_DATA_RECEIVED_FLAG = LOW;
                  DEVICE_E_DATA_RECEIVED_FLAG = LOW;
                */

                sensPoll_TimerA0_Init();
                loraResponseWait_TimerA1_Init();
                sensorPoll_TimerA0_Start();
            }
        }

        if (lora_euart_rx_buffer_rd_ptr >= LORA_EUART_BUFFER_SIZE) {     // Increase read pointer
            lora_euart_rx_buffer_rd_ptr = 0;
        }
    }
}

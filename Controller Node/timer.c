/*
 * timer.c
 *
 *  Created on: Aug 8, 2018
 *      Author: Aaditya Chaudhary
 */
//#define TIMER_PERIOD 0x2DC6    //11718 in decimal

#include "timer.h"
#include <timer_a.h>
#include <timer32.h>
//#include "driverlib.h"
//#include "timer_a.h"
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>


void timerInit(void)
{
    //TimerA Sensor Polling Configuration Parameter --> 40 seconds
    const Timer_A_UpModeConfig sensePoll =
    {
     TIMER_A_CLOCKSOURCE_ACLK,              //ACLK Clock Source
     TIMER_A_CLOCKSOURCE_DIVIDER_64,        //ACLK/1 = 512 Hz
     20480,                                 //20480 tick period = 40 seconds
     TIMER_A_TAIE_INTERRUPT_DISABLE,        //Disable Timer interrupt
     TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE ,   //Enable CCR0 interrupt
     TIMER_A_SKIP_CLEAR                     //Clear value
    };

    MAP_Timer_A_configureUpMode(TIMER_A0_BASE, &sensePoll);

    //TimerA LoRa Response Wait Configuration Parameter --> 10 seconds
    const Timer_A_UpModeConfig loraResponseWait =
    {
     TIMER_A_CLOCKSOURCE_ACLK,              //ACLK Clock Source
     TIMER_A_CLOCKSOURCE_DIVIDER_64,        //ACLK/1 = 512 Hz
     5120,                                  //5120 tick period = 10 seconds
     TIMER_A_TAIE_INTERRUPT_DISABLE,        //Disable Timer interrupt
     TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE ,   //Enable CCR0 interrupt
     TIMER_A_SKIP_CLEAR                     //Clear value
    };

    MAP_Timer_A_configureUpMode(TIMER_A1_BASE, &loraResponseWait);
}


void sensPoll_TimerA0_Init(void)
{
    //TimerA Sensor Polling Configuration Parameter --> 40 seconds
    const Timer_A_ContinuousModeConfig sensePollcont =
    {
     TIMER_A_CLOCKSOURCE_ACLK,              //ACLK Clock Source
     TIMER_A_CLOCKSOURCE_DIVIDER_20,        //ACLK/1 = 1638.4 Hz
     TIMER_A_TAIE_INTERRUPT_ENABLE,         //Enable Timer interrupt
     TIMER_A_DO_CLEAR                     //Clear value
    };

    MAP_Timer_A_configureContinuousMode(TIMER_A0_BASE, &sensePollcont);
}


/* Sensor poll timer start --> poll every 40 seconds */
void sensorPoll_TimerA0_Start(void)
{
    //printf("sensorPoll_TimerA0_Start\n");
    MAP_Timer_A_startCounter(TIMER_A0_BASE, TIMER_A_CONTINUOUS_MODE);
}


void sensPoll_TimerA0_Stop(void)
{
    //printf("sensPoll_TimerA0_Stop\n");
    MAP_Timer_A_stopTimer(TIMER_A0_BASE);
    //MAP_Timer_A_clearTimer(TIMER_A0_BASE);
}


void loraResponseWait_TimerA1_Init(void)
{
    //TimerA LoRa Response Wait Configuration Parameter --> 20 seconds
    const Timer_A_ContinuousModeConfig loraResponseWaitcont =
    {
     TIMER_A_CLOCKSOURCE_ACLK,              //ACLK Clock Source
     TIMER_A_CLOCKSOURCE_DIVIDER_10,         //ACLK/ = 3276.8 Hz
     TIMER_A_TAIE_INTERRUPT_ENABLE,        //Enable Timer interrupt
     TIMER_A_DO_CLEAR                     //Clear value
    };

    MAP_Timer_A_configureContinuousMode(TIMER_A1_BASE, &loraResponseWaitcont);
}


/* LoRa response wait timer start --> 20 seconds */
void loraResponseWait_TimerA1_Start(void)
{
    //printf("loraResponseWait_TimerA1_Start\n");
    MAP_Timer_A_startCounter(TIMER_A1_BASE, TIMER_A_CONTINUOUS_MODE);
}


void loraResponseWait_TimerA1_Stop(void)
{
    //printf("loraResponseWait_TimerA1_Stop\n");
    MAP_Timer_A_stopTimer(TIMER_A1_BASE);
    //MAP_Timer_A_clearTimer(TIMER_A1_BASE);
}


void sensePollTimer32(uint32_t clockTicks)
{
    MAP_Timer32_initModule(TIMER32_0_BASE, TIMER32_PRESCALER_1, TIMER32_32BIT, TIMER32_PERIODIC_MODE);
    MAP_Timer32_setCount(TIMER32_0_BASE, clockTicks);
    MAP_Timer32_enableInterrupt(TIMER32_0_BASE);
    MAP_Timer32_startTimer(TIMER32_0_BASE, false);
}
